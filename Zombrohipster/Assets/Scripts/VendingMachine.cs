﻿using UnityEngine;
using System.Collections;

public class VendingMachine : MonoBehaviour
{

    public GameObject m_pickUp;
	
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
	
	void TakeDamage(DamageInfo t_dmg)
	{
        Debug.Log("VendingMachineHit");
        Instantiate(m_pickUp, transform.position, transform.rotation);
        Destroy(gameObject);
	}
}
