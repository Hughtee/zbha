﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	public float m_hp = 100.0f;

    public void TakeDamage(float t_dmg)
    {
        m_hp -= t_dmg;

        if (m_hp <= 0)
            Destroy(gameObject);
    }
}
