﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]

public class testRotation : MonoBehaviour {
	
	private CharacterController controller;
	
	public float moveSpeed = 6;
	public float turnSpeed = 180;
	public float horizontal;
	public float vertical;


	// Use this for initialization
	void Start () {
		// get the character controller
		controller = gameObject.GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
		// sets horizontal and vertical based on analog input 
		// values range from -1 to +1, midpoint is 0
		horizontal = Input.GetAxis("Horizontal");
		vertical = Input.GetAxis("Vertical");
		
		// set move direction's x-axis and z-axis movement
		// based on horizontal and vertical values
		Vector3 moveDirection = new Vector3(horizontal, 0, vertical );
		
		// set move direction's values relative to the main camera's position
		// movedirection is multiplied by moveSpeed and Time
		moveDirection = Camera.main.transform.TransformDirection(moveDirection) * moveSpeed * Time.deltaTime;
		
		// apply gravity to moveDirection after being set relative to camera's position
		// this is set here to overwrite the y direction if the camera is tilted to a certain degree
		moveDirection = new Vector3(moveDirection.x, -2, moveDirection.z);
		
		// set y-axis rotation relative to mouse movement 
		// we can change this to be relative to keyboard input
		//float yRotation = Input.GetAxis("Mouse X") * turnSpeed * Time.deltaTime;
		
		
		
		var target = transform.position + (moveDirection) ;
		var relativePos = target - transform.position;
		
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(relativePos), turnSpeed * Time.deltaTime) ;
		transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
		
		
//		var rotation = Quaternion.LookRotation(relativePos);
//		rotation = new Quaternion(
//		transform.rotation = rotation;
		
		// apply position and rotation changes to gameObject using the character controller
		controller.Move(moveDirection);
	}
}