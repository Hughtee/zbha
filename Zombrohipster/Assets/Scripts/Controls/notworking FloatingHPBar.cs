﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (GUITexture))]
public class FloatingHPBar : MonoBehaviour {

	public Transform m_Target ; // Entity for hp bar to follow
	public 	Vector3 m_offset = Vector3.up ; // Units in world space, offset for HP bar
	public bool m_clampToScreen = false ; // if true, hp bar will be visible even if object is off sreen
	public float m_clampBorderSize = 0.05f ; // How much viewport to leave at the borders when a label is being clamped
	public bool m_useMainCamera = true ; // Use the camera tagged MainCamera
	public Camera m_cameraToUse ; // Only use this if useMainCamera is false
	
	Camera m_cam ;
	Transform m_thisTransform ;
	Transform m_camTransform ;
	
	// Use this for initialization
	void Start ()
	{
		m_thisTransform = transform ;
		if (m_useMainCamera)
			m_cam = Camera.main ;
		else
			m_cam = m_cameraToUse ;
		
		m_camTransform = m_cam.transform ;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (m_clampToScreen)
		{
			Vector3 t_relativePosition = m_camTransform.InverseTransformPoint(m_Target.position) ;
			t_relativePosition.z = Mathf.Max(t_relativePosition.z, 1.0f) ;
			m_thisTransform.position = m_cam.WorldToViewportPoint(m_camTransform.TransformPoint(t_relativePosition + m_offset)) ;
			m_thisTransform.position = new Vector3(Mathf.Clamp(m_thisTransform.position.x, m_clampBorderSize, 1.0f-m_clampBorderSize),
										 Mathf.Clamp(m_thisTransform.position.y, m_clampBorderSize, 1.0f-m_clampBorderSize),
										 m_thisTransform.position.z);
		}
		
		else
		{
			m_thisTransform.position = m_cam.WorldToViewportPoint(m_Target.position + m_offset);
		}
	}
}
