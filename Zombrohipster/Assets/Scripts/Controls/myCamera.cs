﻿using UnityEngine;
using System.Collections;

public class myCamera : MonoBehaviour {

	public GameObject m_player1 ;
	public GameObject m_player2 ;
	public static GameObject m_cameraFocus ;
	
	public Vector3 m_OffsetAngle = new Vector3 (0, 1, -1) ;
	public float m_OffsetDistanceRatio = 5 ;
	
	// Use this for initialization
	void Start ()
	{
		m_cameraFocus = new GameObject() ;
		transform.position = m_OffsetAngle * m_OffsetDistanceRatio ;
		
		m_OffsetAngle.Normalize() ;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(m_player1.GetComponent<CharacterControllerPlayer>().m_isAlive && m_player2.GetComponent<CharacterControllerPlayer>().m_isAlive)
		{
			Vector3 tempVec = m_player1.transform.position	- m_player2.transform.position ;
			m_cameraFocus.transform.position = m_player2.transform.position + (tempVec / 2) ;
			m_cameraFocus.transform.position = new Vector3 (m_cameraFocus.transform.position.x, 1.58f, m_cameraFocus.transform.position.z) ;
			
			UpdateRatio() ;
			transform.position = Vector3.Lerp(transform.position, m_cameraFocus.transform.position + (m_OffsetAngle * m_OffsetDistanceRatio), 1.0f) ;
			
			transform.LookAt(m_cameraFocus.transform.position) ;
		}
		
		else if(!m_player1.GetComponent<CharacterControllerPlayer>().m_isAlive && m_player2.GetComponent<CharacterControllerPlayer>().m_isAlive)
		{
			m_cameraFocus.transform.position = m_player2.transform.position ;
			m_cameraFocus.transform.position = new Vector3 (m_cameraFocus.transform.position.x, 1.58f, m_cameraFocus.transform.position.z) ;
			
			transform.position = Vector3.Lerp(transform.position, m_cameraFocus.transform.position + (m_OffsetAngle * m_OffsetDistanceRatio), 1.0f) ;
		}
		
		else if(m_player1.GetComponent<CharacterControllerPlayer>().m_isAlive && !m_player2.GetComponent<CharacterControllerPlayer>().m_isAlive)
		{
			m_cameraFocus.transform.position = m_player1.transform.position ;
			m_cameraFocus.transform.position = new Vector3 (m_cameraFocus.transform.position.x, 1.58f, m_cameraFocus.transform.position.z) ;
			
			transform.position = Vector3.Lerp(transform.position, m_cameraFocus.transform.position + (m_OffsetAngle * m_OffsetDistanceRatio), 1.0f) ;
		}
		
	}
	
	void UpdateRatio()
	{
		m_OffsetDistanceRatio = Vector3.Distance(m_player1.transform.position, m_player2.transform.position) * 1.15f ;
		if (m_OffsetDistanceRatio < 15)
			m_OffsetDistanceRatio = 15 ;
	}
	
}
