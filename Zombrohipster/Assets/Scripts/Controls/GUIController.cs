﻿using UnityEngine;
using System.Collections;

enum Players { Player1 = 1, Player2 = 2 };

[RequireComponent (typeof (GUITexture))]

public class GUIController : MonoBehaviour {
	
	public GameObject m_Player1 ;
	public GameObject m_Player2 ;

    private MasterWeapon m_player1Wep;
    private MasterWeapon m_player2Wep;

    public Texture[] m_textures = new Texture[9];
	public Texture ammoTex;
	
	public float healthDif = 0.0f;
	float currentHealth; 
	float lastHealth;
	
	// Use this for initialization
	void Start ()
	{
        m_player1Wep = m_Player1.GetComponent<CharacterControllerPlayer>().m_weapon;
        m_player2Wep = m_Player2.GetComponent<CharacterControllerPlayer>().m_weapon;
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        m_player1Wep = m_Player1.GetComponent<CharacterControllerPlayer>().m_weapon;
        m_player2Wep = m_Player2.GetComponent<CharacterControllerPlayer>().m_weapon;
	}
	
	void OnGUI()
	{
		// Player Health Display
		GUI.TextArea(new Rect (0, 0, 50, 20), "Player1");
		displayHealth((int)Players.Player1);
		
		GUI.TextArea(new Rect (Screen.width - 50, 0, 50, 20), "Player2"); 
		displayHealth((int)Players.Player2);
		
		// Player Weapon & Ammo Display
		GUI.Box(new Rect(0, Screen.height - 100, 100, 100), m_textures[(int)m_player1Wep.m_curWep]);
		GUI.Box( new Rect( 101, Screen.height - 80, 82, 20 ), GUIContent.none );
		displayAmmo((int)Players.Player1);
		
        GUI.TextArea(new Rect(100, Screen.height - 100, 80, 20), "Ammo: " + m_player1Wep.m_curWepInfo.m_curAmmo);
		
		GUI.Box(new Rect(Screen.width - 100, Screen.height - 100, 100, 100), m_textures[(int)m_player2Wep.m_curWep]);
		GUI.Box( new Rect( Screen.width - 183, Screen.height - 80, 82, 20 ), GUIContent.none );
		displayAmmo((int)Players.Player2);
		
		GUI.TextArea(new Rect(Screen.width - 180, Screen.height - 100, 80, 20), "Ammo: " + m_player2Wep.m_curWepInfo.m_curAmmo);
		
		// Player Minimap Display highlight
		// this doesn't resize based on current screen resolution
		// values are only good when game screen is maximized
		//GUI.Box( new Rect( 2, 54, 147, 149 ), GUIContent.none);
		//GUI.Box( new Rect( Screen.width - 149, 54, 148, 149 ), GUIContent.none);	
	}
	
	void displayAmmo( int player_index )
	{
		float k = 0.0f;
		float g = 0.0f;
		
		switch(player_index)
		{
			// this calculation will work for weapons with max ammo being a multiple of 5
			// nail gun = 150
			// molotov = 5
			// ball gun = 5
			// disc = 10
			case (int)Players.Player1:
				k = m_player1Wep.m_curWepInfo.m_maxAmmo / 5.0f;
				if ( k != 0.0f )
					g = (m_player1Wep.m_curWepInfo.m_curAmmo / k );
				int P1A_xPos = 105;
				for( float i = 0; i < g; i++ )
				{
					GUI.Label(new Rect(P1A_xPos, Screen.height - 80, 80, 20), ammoTex );
					P1A_xPos += 15;
				}
				break;
			
			case (int)Players.Player2:
				k = m_player2Wep.m_curWepInfo.m_maxAmmo / 5.0f;
				if ( k != 0.0f )
					g = (m_player2Wep.m_curWepInfo.m_curAmmo / k );
				int P2A_xPos = Screen.width - 119; 
				for( float i = 0; i < g; i++ )
				{
					GUI.Label(new Rect(P2A_xPos, Screen.height - 80, 80, 20), ammoTex );
					P2A_xPos -= 15;
				}
				break;
		}
	}
	
	void displayHealth( int player_index )
	{
		switch(player_index)
		{
			case (int)Players.Player1:
				// create an empty box highlighting the health of the player
				// this box expands if ever the player goes above his current maximum health
				// box stays at default size otherwise
				if( m_Player1.GetComponent<CharacterControllerPlayer>().m_HP <= 100 )
					GUI.Box(new Rect (0, 20,  110, 22), GUIContent.none);
				else
					GUI.Box(new Rect (0, 20,  m_Player1.GetComponent<CharacterControllerPlayer>().m_HP + 10, 22), GUIContent.none);
		
				// checks if the health texture needs to be updated
				// stops health bar from dropping below 0
				if( m_Player1.GetComponent<CharacterControllerPlayer>().m_HP < 0 )
					break;
			
				// draw the health bar
				GUI.DrawTexture(new Rect (5, 24, m_Player1.GetComponent<CharacterControllerPlayer>().m_HP, 14), gameObject.GetComponent<GUITexture>().texture, ScaleMode.ScaleAndCrop) ;
				break;
		
			case (int)Players.Player2:
				// for player2 the difference between the currenthealth and lasthealth before updates
				// needs to be calculated to keep the box and health bar clamped
				// at the end of the screen width
				// otherwise both box and health bar will keep stretching outside of the screen width's boundaries
				float currentHealth = m_Player2.GetComponent<CharacterControllerPlayer>().m_HP;
				float lastHealth = currentHealth - m_Player2.GetComponent<CharacterControllerPlayer>().m_HP;
				healthDif = (currentHealth - lastHealth) - 100.0f;
			
				if( m_Player2.GetComponent<CharacterControllerPlayer>().m_HP <= 100 )
					GUI.Box(new Rect (Screen.width - 110, 20,  110, 22), GUIContent.none);
				
				else
					GUI.Box(new Rect (Screen.width - (m_Player2.GetComponent<CharacterControllerPlayer>().m_HP + 10), 20,  110 + healthDif, 22), GUIContent.none);
				
				if( m_Player2.GetComponent<CharacterControllerPlayer>().m_HP < 0 )
					break;
			
				GUI.DrawTexture(new Rect (Screen.width - (m_Player2.GetComponent<CharacterControllerPlayer>().m_HP + 5), 24, 100 + healthDif, 14), gameObject.GetComponent<GUITexture>().texture, ScaleMode.ScaleAndCrop) ;
				break;
		}
	}
}
