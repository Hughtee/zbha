﻿using UnityEngine;
using System.Collections;

public enum Position {LEFTSIDE = 0, RIGHTSIDE = 1} ;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]

public class CharacterControllerPlayer : MonoBehaviour {
	
	CharacterController controller ;

    Animator anim;
    public float anim_speed = 1.5f;
	
	public int m_index; //init in the inspector
	
	public float m_HP = 100.0f ;
	public bool m_isAlive = true ;
	
	public float m_speed = 5.0f ;
	public float m_jumpSpeed = 0.3f ;
	public float m_gravity = 1.5f ;
	public float m_maxFallSpeed = -5.0f ;
	public Vector3 m_moveDirection ;
	public GameObject m_otherPlayer ;
	public float m_maxDistance = 30.0f ;
	public float m_turnSpeed = 180.0f ;
	
	public bool m_useController ;
    public bool m_isStrafing;
	
	public MasterWeapon m_weapon ;
	
	public Position m_PosRelOthPlayer ; // Position Relative to Other Player
	
	KeyCode left ;
	KeyCode right ;
	KeyCode up ;
	KeyCode down ;
	KeyCode jump ;
	
	// Use this for initialization
	void Start ()
	{
		controller = gameObject.GetComponent<CharacterController>() ;
        anim = GetComponent<Animator>();
		m_moveDirection = Vector3.zero ;
        m_isStrafing = false;
		
		m_weapon = gameObject.GetComponentInChildren<MasterWeapon>() ;
		
		if (gameObject.tag == "Player1")
		{
			left = KeyCode.A ;
			right = KeyCode.D ;
			up = KeyCode.W ;
			down = KeyCode.S ;
			jump = KeyCode.Space ;
			
			m_index = 0;
			//renderer.material.color = Color.magenta ;
		}
		
		else if (gameObject.tag == "Player2Hip")
		{
			left = KeyCode.LeftArrow ;
			right = KeyCode.RightArrow ;
			up = KeyCode.UpArrow ;
			down = KeyCode.DownArrow ;
			jump = KeyCode.Keypad0 ;
			
			m_index = 1;
			
		}
		
		CheckPosition() ;
	}
	
	float GetHP()
	{return m_HP ;}
	
	// Update is called once per frame
	void Update ()
	{
        if(m_isAlive)
		{
			ManagerInput t_input = ManagerInput.Get();
	
	        if (t_input.Dpad_Left_Hit(m_index))
	            m_weapon.SwitchWeapon(WeaponType.MELEE);
	
	        if (t_input.Dpad_UP_Hit(m_index))
	            m_weapon.SwitchWeapon(WeaponType.GUN);
	
	        if (t_input.Dpad_Right_Hit(m_index))
	            m_weapon.SwitchWeapon(WeaponType.PROJ);
	
	        if (t_input.GetLeftTrigger((XInputDotNetPure.PlayerIndex)m_index) > 0.8)
	            m_isStrafing = true;
	        else
	            m_isStrafing = false;
	
	        if (m_weapon.GetAuto())
	        {
	            if (t_input.GetRightTrigger((XInputDotNetPure.PlayerIndex)m_index) > 0.8)
				{
	                m_weapon.Attack();
					anim.SetBool("isShooting", true) ;
				}
				else
				{
					anim.SetBool("isShooting", false) ;
				}
	        }
	        else
	        {
	            if (t_input.RightTriggerHit((XInputDotNetPure.PlayerIndex)m_index))
				{
	                m_weapon.Attack();
					anim.SetBool("isShooting", true) ;
				}
	        }
			
			if(t_input.GetRightTrigger((XInputDotNetPure.PlayerIndex)m_index) == 0.0)
				anim.SetBool("isShooting", false) ;
	        
			
			
			// movement
			m_moveDirection.x = 0 ;
			m_moveDirection.z = 0 ;
			
			// This is input for XBOX controller, don't touch!!!
			if (m_useController)
			{
				Vector2 t_leftStick = t_input.GetLeftStick((XInputDotNetPure.PlayerIndex)m_index) ;
	            m_moveDirection = new Vector3 (t_leftStick.x, m_moveDirection.y, t_leftStick.y) ;
	            float speed = Mathf.Abs(t_leftStick.x) + Mathf.Abs(t_leftStick.y) ;
	            anim.SetFloat("Speed", speed);
	            anim.speed = speed;
			}
			
			if (Input.GetKey(left))
			{
				m_moveDirection.x = -1 ;
				//m_moveDirection.x = 1 ;
				//controller.Move(Vector3.left * speed * Time.deltaTime) ;
			}
			if (Input.GetKey(right))
			{
				m_moveDirection.x = 1 ;
				//controller.Move(Vector3.right * speed * Time.deltaTime) ;
			}
			if (Input.GetKey(up))
			{
				m_moveDirection.z = 1 ;
				//controller.Move(Vector3.forward * speed * Time.deltaTime) ;
			}
			if (Input.GetKey(down))
			{
				m_moveDirection.z = -1 ;
				//controller.Move(Vector3.back * speed * Time.deltaTime) ;
			}
			
			if ((Input.GetKeyDown(jump) || t_input.A_Hit(m_index)) && controller.isGrounded)
			{
				m_moveDirection.y = m_jumpSpeed ;
			}
			
			m_moveDirection = new Vector3(m_moveDirection.x * m_speed * Time.deltaTime, m_moveDirection.y, m_moveDirection.z * m_speed * Time.deltaTime) ;
			
			// depending on the movedirection values, we set the orientation (euler angles/rotation)
			
			Vector3 targetPos = transform.position + (new Vector3(m_moveDirection.x, 0, m_moveDirection.z)) ;
	        Vector2 t_rightStick = t_input.GetRightStick((XInputDotNetPure.PlayerIndex)m_index) ;
	        Vector3 t_direction = new Vector3(t_rightStick.x, 0, t_rightStick.y);
	        if(t_direction == Vector3.zero && !m_isStrafing)
	            t_direction = targetPos - transform.position;
			
	        if (t_direction != Vector3.zero)
	        {
	            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(t_direction), m_turnSpeed * Time.deltaTime);
	            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
	        }
			
			if(m_otherPlayer.GetComponent<CharacterControllerPlayer>().m_isAlive)
			{
				CheckPosition() ;
				
				if (Mathf.Abs(transform.position.x - m_otherPlayer.transform.position.x) >= m_maxDistance)
				{
					if (m_PosRelOthPlayer == Position.LEFTSIDE)
					{
						if(m_moveDirection.x < 0)
							m_moveDirection.x = 0;
					}
					else if (m_PosRelOthPlayer == Position.RIGHTSIDE)
					{
						if (m_moveDirection.x > 0)
							m_moveDirection.x = 0 ;
					}
				}
			}
	
	        //Debug.Log(m_moveDirection);
	        controller.Move(m_moveDirection) ;
			
			if(!controller.isGrounded)
			{
				m_moveDirection.y -= m_gravity * Time.deltaTime ;
				if (m_moveDirection.y <= m_maxFallSpeed)
					m_moveDirection.y = m_maxFallSpeed ;
				Debug.Log("Not Grounded") ;
			}
			else
				m_moveDirection.y = -0.1f ;
			
			
	//		if (transform.position.y < -0.269998f)
	//			transform.position = new Vector3 (transform.position.x, -0.269998f, transform.position.z) ;
	
	        if (anim.speed == 0.0f)
	            anim.speed = anim_speed;
		}
	}
	
	private void CheckPosition()
	{
		if (transform.position.x - m_otherPlayer.transform.position.x > 0)
			m_PosRelOthPlayer = Position.RIGHTSIDE ;
		else if (transform.position.x - m_otherPlayer.transform.position.x < 0)
			m_PosRelOthPlayer = Position.LEFTSIDE ;
	}

    void AddAmmo()
    {
        m_weapon.AddAmmo();
    }

    void Heal()
    {
        m_HP += 20;
        if (m_HP > 100)
            m_HP = 100;
        Debug.Log("Heal");
    }
	
	public void TakeDamage (int t_dmg)
	{
		if(m_HP > 0)
		{
			m_HP -= t_dmg ;
		}
		
		if(m_HP <= 0)
		{
			m_HP = 0 ;
			//gameObject.GetComponentInChildren<MeshRenderer>().enabled = false ;
			//gameObject.SetActive(false) ;
			m_isAlive = false ;
		}
	}
	
	public void OnTriggerEnter(Collider collisionInfo)
	{
		if(collisionInfo.gameObject.layer == LayerMask.NameToLayer("Zombie"))
		{
			TakeDamage(5) ;
		}
	}
}
