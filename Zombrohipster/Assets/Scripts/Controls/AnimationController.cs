﻿using UnityEngine;
using System.Collections;


public class AnimationController : MonoBehaviour {
	
	Animator anim;
	public float anim_speed = 1.5f;
	
	static int idleState = Animator.StringToHash("Base Layer.Idle");
	static int runGunState = Animator.StringToHash("Base Layer.Run and Gun");
	
	AnimatorStateInfo currentBaseState;
	
	public float h;
	public float v;
	
	// Use this for initialization
	void Awake () {
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		h = Input.GetAxis("Horizontal");
		v = Input.GetAxis("Vertical");
		
		anim.SetFloat("Speed", (Mathf.Abs(h)+Mathf.Abs(v)));

        anim.speed = anim_speed;
		currentBaseState = anim.GetCurrentAnimatorStateInfo(0);
	}
}
