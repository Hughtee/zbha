﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (GUITexture))]

public class floatingHPBar : MonoBehaviour {
	
	public GameObject target;
	
	Vector3 offset = new Vector3( 0, 1.15f, 0 );
	public bool clampToScreen = false;
	public float clampBorderSize = .02f;
	
	Camera cam;
	
	// Use this for initialization
	void Start () {	
		
		// we're gonna be using the main camera here
		cam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
		
		if( clampToScreen )
		{
			Vector3 relPos = cam.transform.InverseTransformPoint( target.transform.position );
			
			// set the value of relative pos to whichever is higher between 
			// relPos.z & 1
			relPos.z = Mathf.Max( relPos.z, 1.0f );
			
			// unity script reference
			// transform the position from world space to viewport space
			// Viewport space is normalized and relative to the camera. 
			// The bottom-left of the camera is (0,0); the top-right is (1,1). 
			// The z position is in world units from the camera.
			transform.position = cam.WorldToViewportPoint( cam.transform.TransformPoint(relPos + offset) );
			
			// clamp
			// pos.x is set to be the transform's position.x but no less than .02f and no more than .98f
			// same for pos.y
			transform.position = new Vector3(Mathf.Clamp(transform.position.x, clampBorderSize, 1-clampBorderSize ),
				Mathf.Clamp(transform.position.y, clampBorderSize, 1-clampBorderSize ),transform.position.z);
		}
		
		else
			transform.position = cam.WorldToViewportPoint(target.transform.position + offset);
	
	}
}
