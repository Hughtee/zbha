﻿using UnityEngine;
using System.Collections;

public enum WeaponType {GUN = 0, PROJ = 1, MELEE = 2} ;

// add any weapons you want here
public enum Weapon {NONE = 0, SLEDGE = 1, MOLOTOV = 2, DISC = 3, BIKE_CHAIN = 4,
			        ARM_BAT = 5, NAIL_GUN = 6, PADDLE = 7, BALL_GUN = 8}
public class MasterWeapon : MonoBehaviour
{
	public WeaponType m_curWepType ;
    public Weapon m_curMelWep;
    public Weapon m_curGunWep;
    public Weapon m_curProWep;
    public Weapon m_curWep;
	public WeaponInfo m_curWepInfo ;
	public Transform m_transformForward ;

    const int NUM_WEAPONS = 9;
	
	private Vector3 m_shotDirection ;
    private float m_coolDown = 1.0f ;
	
	// insert prefabs for projectiles here
	public GameObject m_Molotov ;
    public GameObject m_Disc;
	//public GameObject m_standardProjectile ;

    public WeaponInfo[] m_wepInfoList;

    private LineRenderer m_bulletTrail;
	public ParticleEmitter m_bullet ;
	
	// Use this for initialization
	void Start ()
	{
        if (gameObject.tag == "Player1Weapon")
        {
            m_curGunWep = Weapon.BALL_GUN;
            m_curProWep = Weapon.MOLOTOV;
        }

        else if (gameObject.tag == "Player2Weapon")
        {
            m_curGunWep = Weapon.NAIL_GUN;
            m_curProWep = Weapon.DISC;
        }
        
        
        

        m_curWep = m_curGunWep;
        
        m_wepInfoList = new WeaponInfo [NUM_WEAPONS] ;
        for (int i = 0; i < NUM_WEAPONS; i++)
            m_wepInfoList[i] = new WeaponInfo();
        
        m_curWepInfo = m_wepInfoList[(int)m_curWep];

        m_wepInfoList[(int)Weapon.NAIL_GUN].SetInfo(30.0f, 150, 100.0f, 0.1f, false, 0.02f, null);
        m_wepInfoList[(int)Weapon.MOLOTOV].SetInfo(50.0f, 5, 10.0f, 3.0f, false, 0.0f, m_Molotov);
        m_wepInfoList[(int)Weapon.BALL_GUN].SetInfo(10.0f, 500, 100.0f, 0.05f, true, 0.05f, null);
        m_wepInfoList[(int)Weapon.DISC].SetInfo(30.0f, 10, 50.0f, 1.0f, false, 0.0f, m_Disc);

		//m_bullet = gameObject.GetComponent<Particle//
		
        //m_bulletTrail = gameObject.GetComponent<LineRenderer>();
        //m_bulletTrail.SetWidth(0.1f, 0.1f);
        //m_bulletTrail.enabled = false;

        //Debug.Log(LayerMask.NameToLayer("Default"));
	}
	
	// Update is called once per frame
	void Update ()
	{
        if (m_coolDown < m_curWepInfo.m_fireRate)
		{
			//m_bulletTrail.SetPosition(1, Vector3.Lerp(transform.position, (transform.position + m_shotDirection.normalized * ((m_coolDown / m_curWepInfo.m_fireRate) * 1000)), m_coolDown)) ;
			//m_bulletTrail.SetPosition(1, Vector3.Lerp(transform.position, (transform.position + m_shotDirection.normalized * 100), m_coolDown)) ;
			
			m_coolDown += Time.deltaTime;
		}
        else
        {
            m_coolDown = m_curWepInfo.m_fireRate;
            //m_bulletTrail.enabled = false;
        }
	}
	
	public void Attack()
	{
        if (m_coolDown == m_curWepInfo.m_fireRate && m_curWepInfo.m_curAmmo > 0)
        {
            switch (m_curWepType)
            {
                case WeaponType.GUN:
                    m_shotDirection = m_transformForward.forward + Random.insideUnitSphere * m_curWepInfo.m_spread;

                    //m_bulletTrail.enabled = true;
                    //m_bulletTrail.SetPosition(0, transform.position);
					//particleSystem.Emit(
					m_bullet.worldVelocity = m_shotDirection.normalized * 300.0f ;
					m_bullet.Emit() ;

                    RaycastHit t_hit;
                    if (Physics.SphereCast(transform.position - transform.forward, 1.0f, m_shotDirection, out t_hit, 200.0f/*, 9 LayerMask.NameToLayer("Walls")*/))
                    {
                        //m_bulletTrail.SetPosition(1, t_hit.point);
                        if(t_hit.transform.gameObject.GetComponent<BasicZombie>() != null || t_hit.transform.gameObject.GetComponent<VendingMachine>() != null)
						{
							t_hit.transform.gameObject.SendMessage("TakeDamage", m_curWepInfo.getDamageInfo(m_shotDirection.normalized * 6.0f));
						}
					
						
                        //z.TakeDamage(m_curWepInfo.getDamageInfo(m_shotDirection.normalized * 6.0f));
                                                
                        Debug.Log("Hit!");
                    }
                    else
                    {
                        //m_bulletTrail.SetPosition(1, transform.position + m_shotDirection * 200.0f);
                    }

                    m_curWepInfo.m_curAmmo--;
                    break;
                case WeaponType.PROJ:
                    GameObject t_proj = (GameObject)Instantiate(m_curWepInfo.m_weapon, transform.position, transform.rotation);

                    Vector3 t_throwDirection = m_transformForward.forward;
                    //t_throwDirection.y += 0.5f;
                    Debug.Log(t_throwDirection);

                    if (m_curWep == Weapon.MOLOTOV)
                    {
                        t_proj.GetComponent<Molotov>().SetDirection(t_throwDirection, m_curWepInfo.m_speed);
                        t_proj.GetComponent<Molotov>().SetDamage(m_curWepInfo.m_dmg);
                    }

                    else if (m_curWep == Weapon.DISC)
                    {
                        t_proj.GetComponent<Disc>().SetDirection(t_throwDirection, m_curWepInfo.m_speed);
                        t_proj.GetComponent<Disc>().SetDamage(m_curWepInfo.m_dmg);
                    }
                    
                    m_curWepInfo.m_curAmmo--;
                    break;
                case WeaponType.MELEE:
                    break;
            }
            m_coolDown = 0;
        }
	}

    public bool GetAuto()
    {
        return m_curWepInfo.m_isAuto;
    }

    public void SwitchWeapon(WeaponType t_wepType)
    {
        m_curWepType = t_wepType;

        if (m_curWepType == WeaponType.GUN)
        {
            m_curWep = m_curGunWep;
        }
        else if (m_curWepType == WeaponType.MELEE)
        {
            m_curWep = m_curMelWep;
            //gameObject.transform.rotation = Quaternion.
        }
        else if (m_curWepType == WeaponType.PROJ)
        {
            m_curWep = m_curProWep;
        }

        m_curWepInfo = m_wepInfoList[(int)m_curWep];
    }

    public void AddAmmo()
    {
        m_wepInfoList[(int)m_curGunWep].AddAmmo();
        m_wepInfoList[(int)m_curProWep].AddAmmo();
        Debug.Log("AmmoAdded");
    }
}
