﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public float m_lifeSpan = 2.0f ;
	public float m_lifeTimer = 0.0f ;
	
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		m_lifeTimer += Time.deltaTime ;
		if (m_lifeTimer > m_lifeSpan)
			Destroy(gameObject) ;
	}
	
	public void SetDirection (Vector3 t_direction, float t_speed)
	{
		rigidbody.velocity = t_direction * t_speed ;
	}
	
	public void OnCollisionEnter(Collision collisionInfo)
	{
		rigidbody.velocity = Vector3.zero ;
	}
}
