﻿using UnityEngine;
using System.Collections;

public class DamageInfo
{
	public float m_dmg ;
	public Vector3 m_force ;
	
	
	public float Damage
	{
		get{return m_dmg;}
		set{m_dmg = value;}
	}
	
	public Vector3 Force
	{
		get{return m_force;}
		set{m_force = value;}
	}
}
