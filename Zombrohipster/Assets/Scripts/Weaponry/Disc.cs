﻿using UnityEngine;
using System.Collections;

public class Disc : MonoBehaviour
{
    public GameObject m_strikeParticle;
    private float m_Damage = 0.0f;
    public float m_lifeSpan = 2.0f;
    private float m_lifeTimer = 0.0f;
	
	// Update is called once per frame
	void Update ()
    {
        m_lifeTimer += Time.deltaTime;
        if (m_lifeTimer > m_lifeSpan)
            Destroy(gameObject);
	}

    public void SetDirection(Vector3 t_direction, float t_speed)
    {
        rigidbody.velocity = t_direction * t_speed;
    }

    public void SetDamage(float t_dmg)
    { m_Damage = t_dmg; }

    public void OnTriggerEnter(Collider collisionInfo)
    {
        if (collisionInfo.gameObject.tag == "Zombie" || collisionInfo.gameObject.tag == "VendingMachine")
        {
            DamageInfo t_damageInfo = new DamageInfo() ;
			t_damageInfo.Damage = m_Damage ;
			t_damageInfo.m_force = Vector3.zero ;
			
			collisionInfo.SendMessage("TakeDamage", t_damageInfo);
            Debug.Log(collisionInfo.name);
        }
    }
}
