﻿using UnityEngine;
using System.Collections;

public class Molotov : MonoBehaviour {

    public GameObject m_fieryExplosion;
    private float m_Damage = 0.0f;
	// Update is called once per frame
	void Update () {
	
	}

    public void SetDirection(Vector3 t_direction, float t_speed)
    {
        rigidbody.velocity = t_direction * t_speed;
    }

    public void SetDamage(float t_dmg)
    { m_Damage = t_dmg; }

    public void OnCollisionEnter(Collision collisionInfo)
    {
        Instantiate(m_fieryExplosion, transform.position, transform.rotation);
        
        //gameObject.GetComponentInChildren<ParticleSystem>().Play() ;
        

        GameObject[] t_zombies = GameObject.FindGameObjectsWithTag("Zombie");
        GameObject[] t_vMachines = GameObject.FindGameObjectsWithTag("VendingMachine");

        foreach (GameObject zombie in t_zombies)
        {
            if (Vector3.Distance(transform.position, zombie.transform.position) < 5.0f)
            {
                DamageInfo t_damageInfo = new DamageInfo();
                t_damageInfo.Damage = m_Damage;
                t_damageInfo.m_force = Vector3.zero;
                zombie.SendMessage("TakeDamage", t_damageInfo);
            }
        }

        foreach (GameObject vM in t_vMachines)
        {
            if (Vector3.Distance(transform.position, vM.transform.position) < 3.0f)
                vM.SendMessage("TakeDamage", new DamageInfo());
        }

        Destroy(gameObject);
    }
}
