﻿using UnityEngine;
using System.Collections;

public class WeaponInfo {

    public float m_dmg = 0.0f;
    public int m_maxAmmo = 0;
    public int m_curAmmo = 0;
    public float m_speed = 0.0f;
    public float m_fireRate = 0.0f;
    public bool m_isAuto;
    public float m_spread;
    public GameObject m_weapon = null;

    public void SetInfo(float t_dmg, int t_ammo, float t_speed, float t_fireRate, bool t_isAuto, float t_spread, GameObject t_weapon)
    {
        m_dmg = t_dmg;
        m_curAmmo = t_ammo;
        m_maxAmmo = t_ammo;
        m_speed = t_speed;
        m_fireRate = t_fireRate;
        m_isAuto = t_isAuto;
        m_spread = t_spread;
        m_weapon = t_weapon;

        //Debug.Log("Set");
    }
	
	public DamageInfo getDamageInfo(Vector3 t_pushDirection)
	{
		DamageInfo t_damageInfo = new DamageInfo() ;
		
		t_damageInfo.Damage = m_dmg ;
		t_damageInfo.m_force = t_pushDirection ;
		return t_damageInfo ;
	}
	
	public DamageInfo getDamageInfo()
	{
		DamageInfo t_damageInfo = new DamageInfo() ;
		
		t_damageInfo.Damage = m_dmg ;
		t_damageInfo.m_force = Vector3.zero ;
		return t_damageInfo ;
	}

    public void AddAmmo()
    {
        m_curAmmo += m_maxAmmo / 5;
        if (m_curAmmo > m_maxAmmo)
            m_curAmmo = m_maxAmmo;
    }
}
