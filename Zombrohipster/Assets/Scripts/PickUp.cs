﻿using UnityEngine;
using System.Collections;

enum PickUpType { AMMO = 0, HEALTH = 1 } ;

public class PickUp : MonoBehaviour {

    PickUpType m_type;

	// Use this for initialization
	void Start ()
    {
        m_type = (PickUpType) Random.Range(0, 2);
        switch (m_type)
        {
            case PickUpType.AMMO:
                gameObject.renderer.material.color = Color.blue;
                break;
            case PickUpType.HEALTH:
                gameObject.renderer.material.color = Color.green;
                break;
        }

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider t_collisionInfo)
    {
        if (t_collisionInfo.gameObject.tag == "Player1" || t_collisionInfo.gameObject.tag == "Player2")
        {
            if (m_type == PickUpType.AMMO)
                t_collisionInfo.gameObject.SendMessage("AddAmmo");
            else if (m_type == PickUpType.HEALTH)
                t_collisionInfo.gameObject.SendMessage("Heal");
            Destroy(gameObject);
        }
    }
}
