﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZombieSpawner : MonoBehaviour {

    public GameObject m_zombie;

    public float m_spawnTimer = 0.0f;
    public float m_spawnTime = 5.0f;
	
	public float m_initialZval ;
	
	public static List <GameObject> s_zombies ;
	
	//static int s_index ;

    // Use this for initialization
	void Start ()
    {
        m_spawnTimer = 0;
		m_initialZval = transform.position.z ;
		//transform.parent = myCamera.m_cameraFocus.transform ;
		
		if (s_zombies == null)
		{
			s_zombies = new List<GameObject>() ;
			//s_index = 0 ;
		}
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (transform.parent == null)
			transform.parent = myCamera.m_cameraFocus.transform ;
		
		transform.position = new Vector3 (transform.position.x, transform.position.y, m_initialZval) ;
		
        if (m_spawnTimer > 0)
            m_spawnTimer -= Time.deltaTime;
        else
        {
            m_spawnTimer = m_spawnTime;
            SpawnZombie() ;
        }
	}
	
	public void SpawnZombie()
	{
		
		if (s_zombies.Count < 16)
		{
			GameObject t_zombie = (GameObject)Instantiate(m_zombie, transform.position, transform.rotation);
//			t_zombie.SetIndex(s_index) ;
//			s_index++ ;
			s_zombies.Add(t_zombie) ;
		}
	}
	
	public static void RemoveZombie(GameObject t_zombie)
	{
		s_zombies.Remove(t_zombie) ;
	}
}
