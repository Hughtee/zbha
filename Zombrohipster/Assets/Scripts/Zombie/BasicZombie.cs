using UnityEngine;
using System.Collections;

//[RequireComponent(typeof(CharacterController))]

public class BasicZombie : MonoBehaviour {

    Animator m_anim ;
	NavMeshAgent m_navAgent ;
    public GameObject m_target;

    public float m_health = 100.0f;
    //public float m_speed = 5.0f;
	public float m_turnSpeed = 180.0f ;
	
	public float m_maxVel = 6.0f;
	
	public float m_accel = 0.2f;
	public float m_drag = 0.95f;
	
	public Vector3 m_velocity = Vector3.zero ;
	
	//private int m_index ;
	
	// added this for the floating hp bar
	public GUITexture zombieHpBar;
	float m_maxHP = 100.0f;
	float percentOfHP = 100.0f;
	float hpLeft;


	// Use this for initialization
	void Start ()
    {
        m_anim = gameObject.GetComponent<Animator>() ;
		
		m_navAgent = GetComponent<NavMeshAgent>() ;
		
		zombieHpBar = gameObject.GetComponentInChildren<GUITexture>() ;

        //m_target = GameObject.FindGameObjectWithTag("Player2");
		ChoosePlayerTarget();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 tempDir = m_target.transform.position - transform.position;
		m_anim.SetFloat("Distance", tempDir.magnitude) ;
		
//		tempDir.Normalize() ;
//        
//		if (tempDir != Vector3.zero)
//        {
//            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(tempDir), m_turnSpeed * Time.deltaTime);
//            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
//        }
//		
//		m_velocity += tempDir * m_accel ;
//		
//		if (m_velocity.magnitude > m_maxVel)
//			m_velocity = Vector3.Lerp(m_velocity, tempDir * m_maxVel, Time.deltaTime * m_accel) ;
//		
//		m_velocity *= m_drag ;
//		
//		m_controller.SimpleMove(m_velocity) ;
		
		//gameObject.GetComponentInChildren<Renderer>().material.color = Color.Lerp(Color.red, Color.green, m_health / 100);

		if (transform.position.x < -19.0f)
			transform.position = new Vector3 (-19.0f, transform.position.y, transform.position.z) ;
	}
	
	void FixedUpdate()
	{
		ChoosePlayerTarget();
		m_navAgent.SetDestination(m_target.transform.position) ;
	}

    void ChoosePlayerTarget()
    {
		GameObject t_player1 = GameObject.FindGameObjectWithTag("Player1");
        GameObject t_player2 = GameObject.FindGameObjectWithTag("Player2Hip");
		
		if(t_player1.GetComponent<CharacterControllerPlayer>().m_isAlive && t_player2.GetComponent<CharacterControllerPlayer>().m_isAlive)
		{
			if (Vector3.Distance(transform.position, t_player1.transform.position) <
	            Vector3.Distance(transform.position, t_player2.transform.position))
	            m_target = t_player1;
	        else
	            m_target = t_player2;
		}
		
		else if(!t_player1.GetComponent<CharacterControllerPlayer>().m_isAlive && t_player2.GetComponent<CharacterControllerPlayer>().m_isAlive)
		{
			m_target = t_player2 ;
		}
		
		else if(t_player1.GetComponent<CharacterControllerPlayer>().m_isAlive && !t_player2.GetComponent<CharacterControllerPlayer>().m_isAlive)
		{
			m_target = t_player1 ;
		}
	}

    public void TakeDamage(DamageInfo t_dmg)
    {
		// gets the amount of hp the object has left based on percentage
		// of current health % the maximum hp the object can have
		percentOfHP = m_health/m_maxHP;
		hpLeft = percentOfHP * 100;
		
		float currentHP = m_health;
		float lastHealth = currentHP - m_health;
		float hpLost = currentHP - lastHealth;
		
		// set the width of the gui texture - hp bar with the object
		// by changing the pixelInset.width to current value of hpBarLength
		GUITexture temp = zombieHpBar;
		Rect newInset = new Rect(temp.pixelInset.x + (hpLost/(hpLeft/4)), temp.pixelInset.y, hpLeft, temp.pixelInset.height );
		temp.pixelInset = newInset;
		
		zombieHpBar = temp;
		
        m_health -= t_dmg.Damage ;
		
		if (m_velocity.magnitude < m_maxVel)
			m_velocity += t_dmg.Force ;

        if (m_health <= 0)
		{
			ZombieSpawner.RemoveZombie(gameObject) ;
            Destroy(gameObject);
		}
    }
}
