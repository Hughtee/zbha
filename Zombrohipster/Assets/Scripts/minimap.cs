﻿using UnityEngine;
using System.Collections;

public class minimap : MonoBehaviour {
	
	public Transform target;
	public Vector3 minimapPos;
	// Use this for initialization
	void Start () {
		//...
	}
	
	// This will be handled after all other updates
	void LateUpdate () {
		
		transform.position = new Vector3(target.position.x, transform.position.y, target.position.z);
		
		minimapPos = transform.position;
		
		// Clamp
		// all clamp values hardcoded for now
		if( transform.position.x < -10.5f )
			minimapPos.x = -10.5f;
			
		if( transform.position.z < -8.0f )
			minimapPos.z = -8.0f;
		
		if( transform.position.z > 8.0f )
			minimapPos.z = 8.0f;
		
		transform.position = minimapPos;
	}
}
